$(function() {
    var width = 720;
    var Speed = 1200;
    var freeze = 3500;
    var currentSlide = 1;
    var $scroll = $('#scroll');
    var $slideContainer = $('.slides', $scroll);
    var $slides = $('.slide', $scroll);
    var interval;
    function startSlider() {
        interval = setInterval(function() {
            $slideContainer.animate({'margin-left': '-='+width}, Speed, function() {
                if (++currentSlide === $slides.length) {
                    currentSlide = 1;
                    $slideContainer.css('margin-left', 0);
                }
            });
        }, freeze);
    }
    function pauseSlider() {
        clearInterval(interval);
    }
    $slideContainer
        .on('click.simple', pauseSlider)
        .on('dblclick.simple', startSlider);
    startSlider();
});